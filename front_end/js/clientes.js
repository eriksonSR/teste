$(document).ready(function($) {
	//Assim que o documento é carregado pega o estado selecionado e carega as cidades do mesmo
	getCidadesByEstado($("#id_estado").val());

	//Quando muda o estado, obtém as cidades do mesmo
	$("#id_estado").change(function(){
		getCidadesByEstado($(this).val());
	});

	$('#telefone').mask('(00)Z0000-0000', {translation:  {'Z': {pattern: /[0-9]/, optional: true}}});
	$('#pedido').mask('0#');
});

//Obtém um json com a cidades do estado informado
function getCidadesByEstado(estado)
{
	url = base_url + "cidades/aGetCidades/" + estado;
	$.getJSON(url, function( data ) {
        $("#id_cidade").html('');
        //Varre o json obtido e vai preenchendo o select id_cidade
        $(data).each(function(index, el) {
            var option = $("<option value='" + el.id + "'>" + el.cidade + "</option>");
            $("#id_cidade").append(option);
        });
    });
}