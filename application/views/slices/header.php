<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <title>Teste</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="stylesheet" href="<?= base_url('front_end/css/geral.css') ?>">
    <script type="text/javascript" src="<?= base_url('front_end/js/jquery-2.1.4.min.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('front_end/js/libs/jquery.mask.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('front_end/js/geral.js') ?>"></script>
</head>
<body>