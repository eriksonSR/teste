<div id="conteudo">
	<?php
	if ($this->session->flashdata('successo')) {
		echo "<p class='successo'>" . $this->session->flashdata('successo') . "</p>";
	}

	if ($this->session->flashdata('erro')) {
		echo "<p class='erro'>" . $this->session->flashdata('erro') . "</p>";
	}
	?>
	<form action="<?= base_url('index.php/clientes/adicionar') ?>" method="post" id="frm_cad_cliente">
		<legend>Cadastrar cliente:</legend>
		<label for="nome">Nome:</label>
		<input type="text" name="nome" required="" value="<?php echo set_value('nome'); ?>">
		<?php echo form_error('nome'); ?>
		<br>
		<label for="id_estado">Estado:</label>
		<select name="id_estado" id="id_estado" required="">
			<?php
			foreach ($ufs as $uf) {
				echo "<option value='$uf->id'>$uf->uf</option>";
			}
			?>
		</select>
		<br>
		<label for="id_cidade">Cidade:</label>
		<select name="id_cidade" id="id_cidade" required="">
		</select>
		<br>
		<label for="email">Email:</label>
		<input type="text" name="email" required="" value="<?php echo set_value('email'); ?>">
		<?php echo form_error('email'); ?>
		<br>
		<label for="telefone">Telefone:</label>
		<input type="text" name="telefone" id="telefone" required="" value="<?php echo set_value('telefone'); ?>">
		<?php echo form_error('telefone'); ?>
		<br>
		<label for="id_tipo">Tipo:</label>
		<select name="id_tipo" id="id_tipo" required="">
			<?php
			foreach ($tipos_cliente as $tipo) {
				echo "<option value='$tipo->id'>$tipo->tipo</option>";
			}
			?>
		</select>
		<br>
		<label for="pedido">Pedido:</label>
		<input type="text" name="pedido" id="pedido" placeholder="Informe o número do pedido">
		<br>
		<button>Cadastrar</button>
	</form>
</div>
<script type="text/javascript" src="<?= base_url('front_end/js/clientes.js') ?>"></script>