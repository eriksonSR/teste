<?php 

class Estados_model extends CI_Model
{

	//Lista os estados em ordem alfabética
	public function listar()
	{
		$this->db->order_by('uf');
		return $this->db->get('tb_estados')->result();
	}
}