<?php
class Cliente_x_pedidos_model extends CI_Model
{
	public $id_cliente;
	public $pedido;
	
	/**
	* Vincula um pedido a um cliente;
	* @param array $pedido array contendo os dados do pedido
	* @return int
	*/
	public function adicionar($pedido)
	{
		$this->id_cliente = $pedido['id_cliente'];
		$this->pedido = $pedido['pedido'];
		
		return $this->db->insert('tb_cliente_x_pedidos', $this);
	}
}
?>