<?php

class Clientes_model extends CI_Model
{
	public $id_cidade;
	public $id_tipo;
	public $nome;
	public $email;
	public $telefone;

	/**
	* Adiciona um cliente a base de dados e retorna o id do cliente inserido;
	* @param array $cliente array contendo os dados do cliente
	* @return int 
	*/
	public function adicionar($cliente)
	{
		$this->id_cidade = $cliente['id_cidade'];
		$this->id_tipo = $cliente['id_tipo'];
		$this->nome = $cliente['nome'];
		$this->email = $cliente['email'];
		$this->telefone = $cliente['telefone'];
		$this->db->insert('tb_clientes', $this);
		return $this->db->insert_id();
	}

	//Lista os clientes e seus pedidos quando tem pedidos atrelados
	public function listar()
	{
		$this->db->select("cli.nome, cli.telefone, cli.email, p.pedido, c.cidade");
		$this->db->join('tb_cliente_x_pedidos as p', 'cli.id = p.id_cliente', 'left');
		$this->db->join('tb_cidades as c', 'cli.id_cidade = c.id', 'inner');
		$this->db->order_by('nome');
		return $this->db->get('tb_clientes cli')->result();
	}
}