<?php

class Tipos_cliente_model extends CI_Model
{
	//Lista os tipos de cliente em ordem alfabética conforme o tipo
	public function listar()
	{
		$this->db->order_by('tipo');
		return $this->db->get('tb_tipos_cliente')->result();
	}
}