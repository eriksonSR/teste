<?php

class Cidades_model extends CI_Model
{
	/**
	* Lista as cidades pertencentes ao estado informado em ordem alfabética;
	* @param int $id_estado 
	* @return array 
	*/
	public function getCidadesByEstado($id_estado)
	{
		$this->db->where('id_estado', $id_estado);
		$this->db->order_by('cidade');
		return $this->db->get('tb_cidades')->result();
	}
}