<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes extends CI_Controller {
	
	public function __construct() 
	{
        parent::__construct();

        $this->load->model('Estados_model', 'estados');
        $this->load->model('Tipos_cliente_model', 'tipos_cliente');
        $this->load->model('Clientes_model', 'clientes');
        $this->load->model('Cliente_x_pedidos_model', 'cliente_x_pedidos');
    }

    //Quando acessado o controller sem especifica o método é chamado o método cadastrar
    public function index()
    {
    	$this->cadastrar();
    }

    //Carrega a página de cadastrar clientes
    public function cadastrar()
    {
		$dados['ufs'] = $this->estados->listar();
		$dados['tipos_cliente'] = $this->tipos_cliente->listar();
        $dados['menu_ativo'] = "cadastrar";
        $this->load->template("clientes/cadastrar.php", $dados);
    }

    //Adiciona um cliente
    public function adicionar()
    {
    	$this->load->library("form_validation");    
        $this->form_validation->set_rules("nome", "nome", "required");
        $this->form_validation->set_rules("telefone", "telefone", "required");
        $this->form_validation->set_rules("email", "email", "valid_email");
        $this->form_validation->set_error_delimiters("<br><p class='erro'>", "</p>");

        $sucesso = $this->form_validation->run();
        if($sucesso){
	    	$cliente = array('email' => $this->input->post('email'),
								'telefone' => $this->input->post('telefone'),
								'nome' => $this->input->post('nome'),
								'id_tipo' => $this->input->post('id_tipo'),
								'id_cidade' => $this->input->post('id_cidade'));
			$id_cliente = $this->clientes->adicionar($cliente);
			
			if(!empty($id_cliente)){
				$pedido = array('id_cliente' => $id_cliente, 
								'pedido' => $this->input->post('pedido'));
				if($this->cliente_x_pedidos->adicionar($pedido)){
					$this->session->set_flashdata("successo", "Pedido cadastrado com sucesso!");
					redirect(base_url("index.php/clientes/cadastrar/"));
				}else{
					$this->session->set_flashdata("erro", "Ocorreu um erro a vincular o pedido ao cliente!");
					redirect(base_url("index.php/clientes/cadastrar/"));
				}
			}else{
				$this->session->set_flashdata("erro", "Ocorreu um erro ao cadastrar o cliente.");
					redirect(base_url("index.php/clientes/cadastrar/"));
			}
		}else{
			$this->session->set_flashdata("erro", "Você não preencheu os campos corretamente!");
	        $this->cadastrar();
		}
    }

    //Lista clientes e seus pedidos 
    public function listar()
    {
    	$dados['clientes'] = $this->clientes->listar();
        $dados['menu_ativo'] = "listar";
    	$this->load->template("clientes/listar.php", $dados);
    }
}