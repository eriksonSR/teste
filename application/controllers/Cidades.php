<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cidades extends CI_Controller {
	
	public function __construct() 
	{
        parent::__construct();

        $this->load->model('Cidades_model', 'cidades');
    }

    /**
	* Método ajax. Imprime as cidades pertencentes ao estado informado em formato json;
	* @param int $id_estado 
	*/
	public function aGetCidades($id_estado)
	{
		$cidades = $this->cidades->getCidadesByEstado($id_estado);
		echo json_encode($cidades);
	}
}