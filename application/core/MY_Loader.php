<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Loader extends CI_Loader {

    public function template($view, $dados=null) {
        $this->view("slices/header");
        $this->view("slices/menu", $dados);

        if(is_array($view)){
            foreach ($view as $v) {
                $this->view("$v", $dados);
            }
        }else{
            $this->view("$view", $dados);
        }
        $this->view("slices/footer");
    }
    
}